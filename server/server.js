import express from "express";
import morgan from "morgan";
import cors from "cors";
import dotenv from "dotenv";
import connectDB from "./config/db.js";

// dotenv config
dotenv.config();

// Database
connectDB();

//Rest object
const app = express();

// Middlewares
app.use(morgan("dev"));
app.use(express.json());
app.use(cors());

// Routes
app.get("/", (req, res) => {
  return res.status(200).send("<h1>Welcome to Drukstay node server!</h1>");
});

// Port
// const PORT = 8080;
const PORT = process.env.PORT || 8080;

// Listen
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
